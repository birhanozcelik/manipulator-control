
# Gamepad Kullanım Adımları

  - open-manipulator.html dosyasını herhangi bir tarayıcıda aç.
  - Riders'ta editörün navbarına sol tıklayıp "öğeyi denetle"

![GitHub Logo](/images/1.png)

   - console'da şu komutu çalıştır 

```sh
$ document.getElementsByTagName('iframe')[0].src
```


![GitHub Logo](/images/image2.png)
    
- Çıkan url'de "https" sil yerine "wss" yaz
- 8888'i 9090 ile değiştir
- url'in sonundaki "/#/workspace" kısmı sil 
- Bu değişikliklerden sonra elde ettiğin url'i input alanına gir ve "Connect to Riders" butonuna bas.
![GitHub Logo](/images/image1.png)
- Robotu hareket ettirebilmek için Joystick'leri hareket ettir.
- Arm ı hareket ettirebilmek için L1/L2 - R1/R2 tuşlarını kullan
- Gripper ı açıp kapatmak için (Markasız Gamepad için 1-4) (PS için üçgen-daire) (XBox için X-Y) tuşlarını kullanın.
- Bu tuş kombinasyonları modelden modele değişebilir. Robotun hareketlerini gözlemleyip tuşları bulmak en basit ve hızlı yol olacaktır. 


